import errno
import os
from shutil import copyfile

from const import CONST
from utils.json import json_hanlder

_configs = {}


def get_property(key: str):
    if not _configs:
        _load_config()
    return _configs[key]


def _load_config():
    if not os.path.isfile(CONST["FILE_CONFIG_ABSOLUTE"]):
        _create_default_config()
    global _configs
    _configs = json_hanlder.load_json(CONST["FILE_CONFIG_ABSOLUTE"])


def _create_default_config():
    if not os.path.exists(os.path.dirname(CONST["DIR_APP_DATA_ABSOLUTE"])):
        try:
            os.makedirs(os.path.dirname(CONST["DIR_APP_DATA_ABSOLUTE"]))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
    copyfile(CONST["DIR_APP_PATH_ABSOLUTE"] + "/json/default_config.json", CONST["FILE_CONFIG_ABSOLUTE"])
