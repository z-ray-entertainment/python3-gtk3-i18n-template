from const import CONST
from utils.json import json_hanlder

_i18n = {}


def get_text(key: str):
    if not _i18n:
        _load_i18n(CONST["OS_LANG"])
    return _i18n[key]


def _load_i18n(locale: str):
    global _i18n
    try:
        _i18n = json_hanlder.load_json(CONST["DIR_APP_PATH_ABSOLUTE"] + "/json/i18n/" + locale + ".json")
    except FileNotFoundError:
        _i18n = json_hanlder.load_json(CONST["DIR_APP_PATH_ABSOLUTE"] + "/json/i18n/en_GB.json")
